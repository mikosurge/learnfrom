from sqlalchemy.orm import Session

# It is shown that Base should also be imported here for database relations
from models import Base, Question, Choice
import schemas


def create_question(db: Session, question: schemas.QuestionCreate):
    obj = Question(**question.dict())
    db.add(obj)
    db.commit()
    return obj


def get_all_questions(db: Session):
    return db.query(Question).all()


def get_question(db: Session, question_id):
    return db.query(Question).filter(Question.id == question_id).first()


def edit_question(db: Session, question_id, question: schemas.QuestionCreate):
    obj = db.query(Question).filter(Question.id == question_id).first()
    obj.text = question.text
    obj.created_at = question.created_at
    db.commit()
    return obj


def delete_question(db: Session, question_id):
    db.query(Question).filter(Question.id == question_id).delete()
    db.commit()


def create_choice(db: Session, question_id: int, choice: schemas.ChoiceCreate):
    obj = Choice(**choice.dict(), question_id=question_id)
    db.add(obj)
    db.commit()
    return obj


def update_vote(db: Session, choice_id: int):
    obj = db.query(Choice).filter(Choice.id == choice_id).first()
    obj.votes += 1
    db.commit()
    return obj

