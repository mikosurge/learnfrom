from typing import List, Optional
from datetime import datetime

from pydantic import BaseModel, validator


class ChoiceBase(BaseModel):
    text: str
    votes: int = 0


class ChoiceCreate(ChoiceBase):
    pass


class ChoiceList(ChoiceBase):
    id: int

    class Config:
        orm_mode = True


class QuestionBase(BaseModel):
    content: str
    created_at: datetime


class QuestionCreate(QuestionBase):
    created_at: Optional[datetime] = None
    # @validator('created_at', pre=True, always=True)
    # def set_created_at_now(cls, v):
    #     return v or datetime.utcnow()


class Question(QuestionBase):
    id: int

    class Config:
        orm_mode = True


class QuestionInfo(Question):
    choices: List[ChoiceList] = []
