# Polls API
Origin: [Polls API Using FastAPI](https://www.agiliq.com/blog/2020/05/polls-api-using-fastapi/)


## Prerequisite
Make sure a Postgres server at `localhost:5432` is all set up. The username,
password and database name can be changed in `database.py`. Default are
`postgres:paragon` and `agipoll`.
Creating the database with the given name might be required. Using the default
username of `postgres` might requires changing its password to proceed.

```
sudo -iu postgres

psql -U postgres
\password

# Enter new password
\q
exit
```


## auto_now in pydantic and sqlalchemy
pydantic itself provides a way to validate optional value and give it a default value, while following Django pattern the default value should be defined in the
model, not schema.
The correct approach seemed to be leaving pydantic `created_at` field optional, while define the default value in the sqlalchemy model.


```python
# pydantic approach
class QuestionCreate(QuestionBase):
    created_at: Optional[datetime] = None

    @validator('created_at', pre=True, always=True)
    def set_created_at_now(cls, v):
        return v or datetime.utcnow()

# sqlalchemy approach
Column('created_on', DateTime, default=datetime.datetime.now)
Column('last_updated', DateTime, onupdate=datetime.datetime.now)
```


