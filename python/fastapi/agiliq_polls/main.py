from typing import List

from fastapi import FastAPI, HTTPException, Response, Depends
from sqlalchemy.orm import Session

import crud
import schemas
from database import SessionLocal, engine
from models import Base


Base.metadata.create_all(bind=engine)


app = FastAPI()


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


def get_question_obj(db, question_id):
    obj = crud.get_question(db=db, question_id=question_id)
    if obj is None:
        raise HTTPException(status_code=404, detail="Question not found")
    return obj


@app.post('/questions/', response_model=schemas.QuestionInfo)
def create_question(
        question: schemas.QuestionCreate,
        db: Session = Depends(get_db)
        ):
    return crud.create_question(db=db, question=question)


@app.get('/questions/', response_model=List[schemas.Question])
def get_questions(db: Session = Depends(get_db)):
    return crud.get_all_questions(db=db)


@app.get('/questions/{question_id}', response_model=schemas.QuestionInfo)
def get_question(question_id: int, db: Session = Depends(get_db)):
    return get_question_obj(db=db, question_id=question_id)


@app.put('/questions/{question_id}', response_model=schemas.QuestionInfo)
def edit_question(
        question_id: int,
        question: schemas.QuestionCreate,
        db: Session = Depends(get_db),
        ):
    get_question_obj(db=db, question_id=question_id)
    obj = crud.edit_question(db=db, question_id=question_id, question=question)
    return obj


@app.delete('/questions/{question_id}')
def delete_question(question_id: int, db: Session = Depends(get_db)):
    get_question_obj(db=db, question_id=question_id)
    crud.delete_question(db=db, question_id=question_id)
    return {
        'detail': "Question deleted",
        'status_code': 204,
    }


@app.post(
    '/questions/{question_id}/choice',
    response_model=schemas.ChoiceList
)
def create_choice(
        question_id: int,
        choice: schemas.ChoiceCreate,
        db: Session = Depends(get_db)
        ):
    get_question_obj(db=db, question_id=question_id)
    return crud.create_choice(db=db, question_id=question_id, choice=choice)


@app.put('/choices/{choice_id}/vote', response_model=schemas.ChoiceList)
def update_vote(choice_id, db: Session = Depends(get_db)):
    return crud.update_voice(choice_id=choice_id, db=db)
