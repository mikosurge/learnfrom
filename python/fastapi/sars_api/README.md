# SARS API
This project is based on [Building a Data API with FastAPI and SQLAlchemy](https://towardsdatascience.com/fastapi-cloud-database-loading-with-python-1f531f1d438a)

## Common project structure
```bash
├── crud.py
├── database.py
├── main.py
├── models.py
└── schemas.py
```
