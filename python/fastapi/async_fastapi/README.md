# Async FastAPI
This example is based on [FastAPI with SQLAlchemy, PostgreSQL, Alembic and Docker](https://ahmed-nafies.medium.com/tutorial-fastapi-sqlalchemy-postgresql-alembic-and-docker-part-2-asynchronous-version-8a339ce97e6d).


## Alembic notes
To do migrations, alembic requires `sqlalchemy.url` to be defined.
This example used a workaround to trickly `import models` from the parent folder, whilst the project's supposed to be structures better.

Consider running `alembic` in a folder inside the app project, so that the alembic `env.py` can see other modules.

It is also required to declare the metadata, either from `sqlalchemy.MetaData()` or `models.Base.metadata`, where `Base = declarative_base()`.
