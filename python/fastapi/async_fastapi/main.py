from models import User as UserModel
from schema import User as UserSchema
from app import app


@app.post('/users/')
async def create_user(user: UserSchema):
    user_id = await UserModel.create(**user.dict())
    return {'user_id': user_id}


@app.get('/users/{id}', response_model=UserSchema)
async def get_user(id: int):
    user = await UserModel.get(id)
    return UserSchema(**user).dict()
