import nltk
from newspaper import Article


# URL = ('http://fox13now.com/2013/12/30/'
#     'new-year-new-laws-obamacare-pot-guns-and-drones/')
URL = 'https://foo.bar'


def generate_summary(url):
    article = Article(url)
    article.download()
    article.parse()

    try:
        nltk.data.find('tokenizers/punkt')
    except LookupError:
        nltk.download('punkt')
    finally:
        article.nlp()

    summary = article.summary
    print(summary)


if __name__ == '__main__':
    print(generate_summary(URL))
