import logging
import os

from fastapi import FastAPI
from tortoise import Tortoise, run_async
from tortoise.contrib.fastapi import register_tortoise


logger = logging.getLogger(__name__)


def init_db(app: FastAPI) -> None:
    register_tortoise(
        app,
        db_url=os.environ.get('DATABASE_ENDPOINT'),
        modules={'models': ['app.models.tortoise']},
        generate_schemas=False,
        add_exception_handlers=True,
    )


async def generate_schemas() -> None:
    logger.info('Initializing Tortoise...')

    await Tortoise.init(
        db_url=os.environ.get('DATABASE_ENDPOINT'),
        modules={'models': ['app.models.tortoise']},
    )
    logger.info("Generating schemas via Tortoise...")
    await Tortoise.generate_schemas()
    await Tortoise.close_connections()


if __name__ == '__main__':
    run_async(generate_schemas())

