# Testdriven TDD Fastapi
This project is cloned the parent [testdriven.io course](https://testdriven.io/courses/tdd-fastapi/), with modifications to suit my own stack.
Some alternate options are taken into consideration:
- `poetry` instead of `requirements`
- `sqlalchemy` instead of `tortoise-orm`

The original implementation is still being kept, while the individual implementation is added for future reference.


## Project structure

## Technical stack

## Prerequisite
Due to lacking documentation and the official guidance for this project, other additional parts are recommended for reference.
- heroku deployment with github, from [The Complete Node.js Developer Course (3rd Edition)](https://www.udemy.com/course/the-complete-nodejs-developer-course-2/)
- Building and testing from Docker images, from [Build a Backend REST API with Python & Django - Advanced](https://www.udemy.com/course/django-python-advanced/)

### Testing on Docker images
A couple of setup steps are required to ensure the testing execution is smooth.
#### Make sure the original docker image is running good
Any `entrypoint.sh` has to be `chmod`-ed before copying to the docker image.
Build the very first Docker image using:
```sh
DOCKER_BUILDKIT=1 docker build .
```
Once the image is built successfully, proceed to the next step.

#### Upgrade docker-compose to the latest version
The docker-compose `1.21.0` by default can not run scripts of version `3.8`. Consider upgrading docker-compose as an essential requirement.
```sh
sudo wget -O /usr/local/bin/docker-compose https://github.com/docker/compose/releases/download/1.25.5/docker-compose-Linux-x86_64
sudo chmod +x /usr/local/bin/docker-compose
```

#### Migrate to docker-compose
Create a docker-compose file that contains all the services required.
Consider a lean start with `alpine` images. `docker-compose build` will do the rest.
```sh
docker-compose build
```
A success on building indicates a good sign.

#### Individually testing containers
Any command can be individually injected into the container using:
```sh
docker-compose run <service> sh -c "<command>"
```

## Important notes
- The `main.yml`, `Dockerfile.prod` and `release.sh` are just merely copied, no modification or testing have been made.
