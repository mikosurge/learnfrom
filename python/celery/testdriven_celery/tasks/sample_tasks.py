import time

from celery import shared_task


@shared_task
def create_task(time_interval):
    time.sleep(int(time_interval) * 10)
    return True
