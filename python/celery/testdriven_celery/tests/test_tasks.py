import json
from unittest.mock import patch

from django.urls import reverse

from tasks import sample_tasks


HOME_URL = reverse('home')
RUN_TASK_URL = reverse('run_task')


def test_home(client):
    """Test visiting home URL returns code 200.
    """
    response = client.get(HOME_URL)
    assert response.status_code == 200


def test_task_status(client):
    """Test triggering a task returns its ID, the status should be
    PENDING while not finished and SUCCESS upon completion.
    """
    response = client.post(RUN_TASK_URL, {'type': 0})
    content = json.loads(response.content)
    task_id = content['task_id']
    assert response.status_code == 202
    assert task_id

    response = client.get(reverse('get_status', args=[task_id]))
    content = json.loads(response.content)

    # TODO Should provide a better way of checking object similarity
    assert content == {
        'task_id': task_id,
        'task_status': 'PENDING',
        'task_result': None,
    }
    assert response.status_code == 200

    while content['task_status'] == 'PENDING':
        response = client.get(reverse('get_status', args=[task_id]))
        content = json.loads(response.content)
    assert content == {
        'task_id': task_id,
        'task_status': 'SUCCESS',
        'task_result': True,
    }


def test_task():
    assert sample_tasks.create_task.run(1)
    assert sample_tasks.create_task.run(2)
    assert sample_tasks.create_task.run(3)


@patch('tasks.sample_tasks.create_task.run')
def test_mock_task(mock_run):
    assert sample_tasks.create_task.run(1)
    sample_tasks.create_task.run.assert_called_once_with(1)

    assert sample_tasks.create_task.run(2)
    assert sample_tasks.create_task.run.call_count == 2

    assert sample_tasks.create_task.run(3)
    assert sample_tasks.create_task.run.call_count == 3
