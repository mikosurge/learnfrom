import time

from worker import app


@app.task
def add(a, b):
    time.sleep(2)
    return a + b


@app.task
def xsum(numbers):
    return sum(numbers)
