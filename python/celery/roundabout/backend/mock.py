from worker import app
from celery.execute import send_task
from celery import chain, chord, Signature


# Both ways are correct
print('Using signature callable to create Signature of the task')
print(app.signature('tasks.add', args=(2, 3), queue='add_tasks').delay().get())

print('Using send_task with the Celery app instance only')
print(app.send_task('tasks.add', args=(2, 3), queue='add_tasks').get())

print('Importing send_task directly and use it without the Celery app')
print(send_task('tasks.add', args=(2, 3), queue='add_tasks').get())

# Chaining tasks
print('Chaining tasks add, substract, multiply, divide')
res = chain(
    Signature('tasks.add', args=(4, 8), queue='add_tasks'),
    Signature('tasks.substract', args=(6,), queue='substract_tasks'),
    Signature('tasks.multiply', args=(3,), queue='multiply_tasks'),
    Signature('tasks.divide', args=(2,), queue='divide_tasks'),
)()
print(res.get())

# Note that () is a substitution of delay()
print('Chaining immutable tasks add, substract, multiply, divide')
res = chain(
    Signature('tasks.add', args=(4, 8), queue='add_tasks'),
    Signature(
        'tasks.substract',
        args=(6, 3),
        queue='substract_tasks',
        immutable=True,
    ),
    Signature(
        'tasks.multiply',
        args=(3, 10),
        queue='multiply_tasks',
        immutable=True,
    ),
    Signature(
        'tasks.divide',
        args=(14, 7),
        queue='divide_tasks',
        immutable=True,
    ),
)()
print(res.get())

print('Doing chord to calculate total value')
xsum = chord(
    (
        Signature('tasks.add', (i, i), queue='add_tasks')
        for i in range(10)
    ),
    Signature('tasks.xsum', (), queue='add_tasks')
)()
print(xsum.get())

print('Using shorthand for chaining')
res = (
    Signature('tasks.add', args=(2, 2), queue='add_tasks') |
    Signature('tasks.multiply', args=(8,), queue='multiply_tasks') |
    Signature('tasks.multiply', args=(10,), queue='multiply_tasks')
)()
print(res.get())

print('Using shorthand for chord')
xsum = chord(
    Signature('tasks.add', args=(i, i), queue='add_tasks')
    for i in range(100)
)(
    Signature('tasks.xsum', (), queue='add_tasks')
)
print(xsum.get())
