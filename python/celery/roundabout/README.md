# Roundabout
This repository is a quick demo of multiple sub Celery workers constantly receiving tasks pushed from the main worker to complete a hypothetic complex calculation.
The main point of creating this `learnfrom` project is to test out the routing functionality of Celery, in which a task can be trigger by just calling its name, so that it would take less effort to import the task's source code and construct the project structure.

There are 4 microservices for adding, substracting, multiplying and dividing numbers, created for simulating separated celery worker services.
The source code provided for each service are quite similar. However for the sake of simulating, it's got duplicated.


## Techstack used
- RabbitMQ, as the broker
- Redis, as the result backend
- Celery, as the task queue
- FastAPI, as the backend


## How it works
The backend should expose an API to receive a POST request, but right now it holds only one demo having several examples of how to run and ochestrate Celery tasks.
To keep it alive, this command keeps looping inside `backend`:
```sh
while true; do sleep 1; done
```

### Creating containers
Simply run `docker-compose up -d --build` to automatically build and run all services. In case of rebuilding a single container, add its service name after `--build`.

### Running demo
The demo is `mock.py`, located in `backend` service. Therefore, `exec sh` command in the `backend` container.
```sh
docker-compose exec backend sh
```
Then proceed with:
```sh
python mock.py
```
That will do all examples.


## Next steps
For more advanced and fancy uses of Celery functions, navigate to its main documentation.
The repository is just a proof of routing tasks in Celery.

