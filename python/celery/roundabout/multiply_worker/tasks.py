from worker import app


@app.task()
def multiply(a, b):
    return a * b
