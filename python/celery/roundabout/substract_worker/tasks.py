from worker import app


@app.task()
def substract(a, b):
    return a - b
