import os

broker_url = os.environ.get(
    'CELERY_BROKER_URL',
    'amqp://guest@rmq:5672//'
)

result_backend = os.environ.get(
    'CELERY_RESULT_BACKEND',
    'redis://redis:6379/0/'
)

task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']

imports = ('tasks', )

task_routes = {
    'tasks.divide': os.environ.get('WORKER_QUEUE', 'default'),
}

task_default_queue = 'default'
task_default_exchange = 'default'
task_default_routing_key = 'default'
